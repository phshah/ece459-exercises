// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    for i in 0..number {
        if i%multiple1 == 0 || i%multiple2 == 0 {
            sum = sum + i;
        }
    }
    sum
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
